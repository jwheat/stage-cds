/*
	DATAVORE PROTOTYPE
*/
var result = "";
var infos = "";
var numeric_table = dv.table();
var tab = dv.table();
var count = 0;
var filename;
var start;
// import data
var input;

function readMultipleFiles(evt) {
    start = new Date().getTime();
    var files = evt.target.files;
    if (files) {
        for (var i = 0, f; f = files[i]; i++) {
            filename = f;
            var r = new FileReader();
            r.onloadend = function (e) {
                input = e.target.result;
                process();

                repaintInfos();
            };
            r.readAsText(f);
        }
    } else {
        alert("Failed to load files");
    }
}

document.getElementById('fileinput').addEventListener('change', readMultipleFiles, false);

// main function
var label = [];
var label_num = [];
function process() {
    var content = $.csv.toArrays(input);

    var search = "";
    //content = invertArray(content);
    label = content[0];

    count = content.length - 1;
    for (var j = 0; j < content[0].length; j++) {
        var col = content.map(function (value, index) {
        	if(isFinite(value[j]))
            	return parseFloat(value[j]);
            return value[j];
        });
        col.shift();
        var coltype = isNumberArray(col.slice(0));

        if (coltype == 1) {
            tab.addColumn(label[j], col.slice(), dv.type.numeric);
            numeric_table.addColumn(label[j], col.slice(), dv.type.numeric);
            label_num.push(label[j]);
        } else if (coltype == 2)
            tab.addColumn(label[j], col, dv.type.nominal);
        else
            tab.addColumn(label[j], col, dv.type.unknown);
    }

    var end = new Date().getTime();
    var time = end - start;

    generate_menu();
    infos += "Load data : " + time + "ms <br/>";

    var end2 = new Date().getTime();
    var time = end2 - end;

    infos += "Show data : " + time + "ms <br/>";

    search += "<form name='searchForm' onsubmit='return validateForm()' method='post'><select name='field' size='1'>"

    for (var i = 0; i < label.length; i++) {
        search += "<option>" + label[i];
    }

    search += "</select><input name='value'/><input type='submit' value='submit'/></form>";

    $("#search").html(search);

}

// to print results
function printTable(array) {
    var print = "<table border=1>";
    print += "<tr>"
    for (var i = 0; i < label.length; i++) {
        print += "<th>" + label[i] + "</th>";
    }
    print += "</tr>";
    for (var i = 0; i < array[0].length; i++) {
        print += "<tr>";
        for (var j = 0; j < array.length; j++) {
            //document.write(array);
            print += "<td>" + array.get(j, i) + "</td>";
        }
        print += "</tr>";
    }
    print += "</table>";
    return print;
}

function printTable2(array) {

    var print = "<table id=\"result_table\" border=1>";
    print += "<tr>"
    for (var i = 0; i < label.length; i++) {
        print += "<th>" + label[i] + "</th>";
    }
    print += "</tr>";
    for (var i = 0; i < array[1].length; i++) {
        print += "<tr>";
        for (var j = 0; j < array.length; j++) {
            //document.write(array);
            print += "<td>" + array[j][i] + "</td>";
        }
        print += "</tr>";
    }
    print += "</table>";
    return print;
}

var ftab;

function validateForm() {
    var start = new Date().getTime();
    result = "";
    var field = document.forms["searchForm"]["field"].value;
    var value = document.forms["searchForm"]["value"].value;

    // control if the value is not empty
    if (value == null || value == "") {
        alert("Veuillez entrer une valeur");
    }

    // good case
    else {

        // datavore query
        ftab = tab.where(function (table, row) {
            return table.get(field, row) == value;
        });

        var end = new Date().getTime();
        var time = end - start;

        infos += "Search : " + time + "ms <br/>";

        // print the results
        if (ftab[0].length > 0) {
            result += ftab[0].length + " rows founds (over " + count + " ) <br/>";

            if (ftab[0].length < 10000)
                result += printTable2(ftab);
            else {
                result += "To many rows ; print disabled. <br/>";
            }
        } else {
            result += "No result found ! <br/>";
        }

        $("#result").html(result);

        // performance measures

        repaintInfos();
    }

    return false;
}

function repaintInfos() {
    $("#infos").html(infos);
}

function isNumber(v) {
    return isFinite(v);
}

function isNumberArray(array) {
    if (array.length == 1 && (array[0] == "" || array[0] == null || array[0] == undefined))
        return 0; // empty column
    if (array[0] == "" || array[0] == null || array[0] == undefined) {
        array.shift();
        return isNumberArray(array);
    }
    if (isNumber(array[0]))
        return 1; // number column
    else
        return 2; // string column
}

function roundDec(nombre, precision) {
    var precision = precision || 2;
    var tmp = Math.pow(10, precision);
    return Math.round(nombre * tmp) / tmp;
}

function generate_menu() {
    var menu = "<form name=\"menu\"><select name=\"col_menu\" multiple>";
    for (var i in label_num) {
        menu += "<option ";
        if (i < 2)
            menu += "selected";
        menu += "> " + label_num[i];
    }
    menu += "</select><br/>";

    menu += "Box size : <input name='box_size' value='450'/><br/>";

    menu += "Radius : <input name='radius' value='8'/><br/>";

    menu += "<button type=\"button\" onclick=\"plot()\">Plot</button>";

    menu += "</form>";
    $("div#menu").html(menu);

}

function plot() {
    var cols = [];
    for (i = 0; i < document.forms.menu.col_menu.options.length; i++) {
        if (document.forms.menu.col_menu.options[i].selected) {
            cols.push(document.forms.menu.col_menu.options[i].text);
        }
    }

    var box_size = document.forms["menu"]["box_size"].value;
    var radius = document.forms["menu"]["radius"].value;

    $('#overlay').bind('Custom.StartWork', function () {
        $(this).html("<h1>Loading..</h1>");
        $(this).show();
    });

    $('#overlay').bind('Custom.EndWork', function () {
        $(this).hide();
    });

    $('#overlay').trigger('Custom.StartWork');
    setTimeout(function () {
        ShowPlots();
    }, 0);

    function ShowPlots() {
        generate_matrix(cols, box_size, radius);

        $('#overlay').trigger('Custom.EndWork');
    }
}

function plotSearch(i,j,xbi,ybi,xbs,ybs) {
	//console.log("i : "+i+", label_i : "+label_num[i]+", j : "+j+", label_j : "+label_num[j]+", xbi : "+xbi+", ybi : "+ybi+", xbs : "+xbs+", ybs : "+ybs);
    var resultTab = tab.where(function (table, row) {
        return table.get(label_num[i], row) > xbi
        && table.get(label_num[j], row) < ybs
        && table.get(label_num[j], row) > ybi
        && table.get(label_num[i], row) < xbs;
    });

    var result = printTable2(resultTab);
    $('#result').hide();


    $('#result').html(result);

    $('#result').smartpaginator({
        totalrecords: resultTab[0].length,

        recordsperpage: 15,

        datacontainer: 'result_table',

        dataelement: 'tr',
        theme: 'green'
    });
    $('#result').show();

    return resultTab;
}