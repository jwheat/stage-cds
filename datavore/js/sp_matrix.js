var size = 200,
    radius = 5;

var hextab;
var checked;
var retest;

function generate_matrix(cols, box_size, radius_size) {
    $("#matrix").html("");

    hextab = dv.table();
    checked = [];

    for (var i in label_num) {
        checked[label_num[i]] = false;
    }

    size = box_size;
    radius = radius_size;

    var start = new Date().getTime();

    var end = new Date().getTime();
    var time = end - start;

    tlength = numeric_table.length;

    numeric_table.deleteNAs();

    table = numeric_table.slice();

    var balises = "<table>";
    balises += "<tr><th></th>";

    for (var i in label_num)
        if (_.contains(cols, label_num[i]))
            balises += "<th>" + label_num[i] + "</th>";

    balises += "</tr>";
    for (var i = 0; i < tlength; i++) {
        if (_.contains(cols, label_num[i])) {
            balises += "<tr>";
            balises += "<td><span class=\"left_label\"><strong>" + label_num[i] + "</strong></span></td>";
            for (var j = 0; j < tlength; j++) {
                if (_.contains(cols, label_num[j]))
                    balises += "<td><div id=\"d_" + i + "_" + j + "\" class=\"chart\"></div></td>";
            }
            balises += "</tr>";
        }
    }

    balises += "</table>";

    $("#matrix").html(balises);

    var start1 = new Date().getTime();

    data = [];
    for (var i = 0; i < table.length; i++) {
        data[i] = [];
        if (_.contains(cols, label_num[i]))
            for (var k = 0; k < table.length; k++) {
                temp = [];
                if (i != k && _.contains(cols, label_num[k])) {
                    for (var j = 0; j < table[0].length; j++) {
                        if (table[i][j] != undefined && table[k][j] != undefined)
                            temp.push([parseFloat(table[i][j]), parseFloat(table[k][j])]);
                    }
                }
                data[i].push(temp.slice());
            }
    }

    var end1 = new Date().getTime();
    var time1 = end1 - start1;

    infos += "** PrePlot data : " + time1 + "ms <br/>";

    var start2 = new Date().getTime();

    for (var i = 0; i < tlength; i++)
        if (_.contains(cols, label_num[i]))
            for (var j = 0; j < tlength; j++)
                if (j != i && _.contains(cols, label_num[j])) {
                    draw_block(data[i][j], "d_" + i + "_" + j + "");
                }

    var end2 = new Date().getTime();
    var time2 = end2 - start2;

    infos += "** Plot hexbins : " + time2 + "ms <br/>";

    $("#infos").html(infos);

    var start2 = new Date().getTime();

    for (var i = 0; i < tlength; i++)
        if (_.contains(cols, label_num[i]))
            draw_histo(numeric_table[i], "d_" + i + "_" + i + "");

    var end2 = new Date().getTime();
    var time2 = end2 - start2;

    infos += "** Plot histo : " + time2 + "ms <br/>";

    $("#infos").html(infos);

}


var numPoints = 1000,
    hexset,
    countMax,
    scale,
    width = 200,
    height = 200,
    numClasses = 9,
    cbScheme = "Reds",
    hexI = 30;
var x, y;
var brushActive;
var scales = [];

var prev_extent;

function draw_block(src, div) {
    var i = div.charAt(2);
    var j = div.charAt(4);

    var start = new Date().getTime();
    var margin = {
            top: 20,
            right: 20,
            bottom: 20,
            left: 40
        },
        width = size - margin.left - margin.right,
        height = size - margin.top - margin.bottom;

    var points = src.slice();

    var hexbin = d3.hexbin()
        .x(function (d) {
            return x(d[0]);
        })
        .y(function (d) {
            return y(d[1]);
        })
        .size([width, height])
        .radius(radius);

    var min0 = d3.min(points, function (d) {
        return d[0];
    });
    var max0 = d3.max(points, function (d) {
        return d[0];
    });

    var min1 = d3.min(points, function (d) {
        return d[1];
    });
    var max1 = d3.max(points, function (d) {
        return d[1];
    });


    var padding0 = (max0-min0)/100*10;
    var padding1 = (max1-min1)/100*10;

    var x = d3.scale.linear()
        .domain([min0-padding0, max0+padding0])
        .range([0, width]);

    var y = d3.scale.linear()
        .domain([min1-padding1, max1+padding1])
        .range([height, 0]);

    scales[i + j + "x"] = x;
    scales[i + j + "y"] = y;

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .tickSize(-height);

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickSize(-width);

    var svg = d3.select("div#" + div).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var hextemp = [];

    var isNaN = false;
    var point = svg.selectAll(".point")
        .data(hexbin(points))
        .enter().append("path")
        .attr("class", "point")
        .attr("d", hexbin.hexagon())
        .attr("transform", function (d) {
            hextemp.push(x.invert(d.x));
            return "translate(" + d.x + "," + d.y + ")";
        })
        .style("fill", function (d) {
            var color = d3.scale.linear()
                .domain([0, Math.floor(Math.sqrt(countMax))])

            .range(["#deebf7", "#c6dbef", "#9ecae1", "#6baed6", "#4292c6", "#2171b5", "#08519c", "#08306b"])
                .interpolate(d3.interpolateLab);
            if (isNaN)
                return "white";
            return color(d.length);
        });

    var current_indice = div.charAt(2);
    if (!checked[label_num[current_indice]])
        hextab.addColumn(label_num[current_indice], hextemp.slice(), dv.type.numeric);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    var brush = d3.svg.brush()
        .x(d3.scale.identity().domain([0, width]))
        .y(d3.scale.identity().domain([0, height]))
        .on("brushend", brushed);

    svg.append("g")
        .attr("class", "brush")
        .call(brush);

    var MT;
    var firstbrush = true;
    var can_be_search = false;
    var timeout;

    function brushed() {
        var e = brush.extent();
        point.classed("selected", function (d) {
            return d["x"] <= (e[1][0]) && d["x"] >= (e[0][0]) && d["y"] <= (e[1][1]) && d["y"] >= (e[0][1]);
        });
        var xbi = roundDec(x.invert(e[0][0]));
        var xbs = roundDec(x.invert(e[1][0]));
        var ybs = roundDec(y.invert(e[0][1]));
        var ybi = roundDec(y.invert(e[1][1]));

        plotSearch(i, j, xbi, ybi, xbs, ybs);
        //updatePlots(xbi, xbs, ybi, ybs);
        //d3.selectAll(".brush").call(brush.clear());
    }

    function updatePlots(xbi, xbs, ybi, ybs) {
        console.log("i : " + i + ", j : " + j + ", xbi : " + xbi + ", ybi : " + ybi + ", xbs : " + xbs + ", ybs : " + ybs);
        console.log("i : " + i + ", j : " + j + ", xbi : " + x(xbi) + ", ybi : " + y(ybi) + ", xbs : " + x(xbs) + ", ybs : " + y(ybs));
        /*      var resultTab = hextab.where(function (table, row) {
        return table.get(label_num[i], row) > xbi
        && table.get(label_num[i], row) < xbs
        && table.get(label_num[j], row) > ybi1
        && table.get(label_num[j], row) < ybs1;
        });
    */
        /*    var sel = d3.select("body").selectAll("points")
        .attr("transform", function(d) { 
            var tab = d.substring(10,d.length-1).split(",");
            return tab[0] <= xbs && tab[0] >= xbi && tab[1] <= ybs && tab[1] >= ybi;
        });

        sel.classed("selected", true);
        console.log(sel);
    */
    }
};

function draw_histo(src, div) {
    // Generate a Bates distribution of 10 random variables.
    var values = src.slice();

    // A formatter for counts.
    var formatCount = d3.format(",.0f");

    var margin = {
            top: 10,
            right: 30,
            bottom: 30,
            left: 45
        },
        width = size - margin.left - margin.right,
        height = size - margin.top - margin.bottom;

    var maxX = d3.max(values, function (d) {
        return d;
    });

    var minX = d3.min(values, function (d) {
        return d;
    });

    var padding = (maxX-minX)/100*10;

    var x = d3.scale.linear()
        .domain([minX-padding, maxX+padding])
        .range([0, width]);

    // Generate a histogram using twenty uniformly-spaced bins.
    var data = d3.layout.histogram()
        .bins(x.ticks(20))
        (values);


    var maxY = d3.max(data, function (d) {
        return d.y;
    })

    var y = d3.scale.linear()
        .domain([0, maxY])
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    var numbins = data.length;

    var svg = d3.select("div#" + div).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var bar = svg.selectAll(".bar")
        .data(data)
        .enter().append("g")
        .attr("class", "bar")
        .attr("transform", function (d) {
            return "translate(" + x(d.x) + "," + y(d.y) + ")";
        });

    bar.append("rect")
        .attr("x", 1)
        .attr("width",(width+2*padding)/ numbins)
        .attr("height", function (d) {
            return height - y(d.y);
        });

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);
}