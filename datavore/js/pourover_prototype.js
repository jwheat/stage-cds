/*
	DATAVORE PROTOTYPE
*/
var result = "";
var infos = "";
var numeric_table = dv.table();
var tab = dv.table();
var count = 0;
var filename;
var start;
// import data
var input;

function readMultipleFiles(evt) {
    start = new Date().getTime();
    var files = evt.target.files;
    if (files) {
        for (var i = 0, f; f = files[i]; i++) {
            filename = f;
            var r = new FileReader();
            r.onloadend = function (e) {
                input = e.target.result;
                process();
            };
            r.readAsText(f);
        }
    } else {
        alert("Failed to load files");
    }
}

document.getElementById('fileinput').addEventListener('change', readMultipleFiles, false);

// main function
var label = [];
var data = [];
function process() {
    var content = $.csv.toArrays(input);

    var search = "";

    label = content[0];
    label_num = [];

    count = content.length - 1;
    content.shift();
    for (var i in content) {
        var temp = {};
        for(var j in content[i]) {
            temp[label[j]] = content[i][j];
        }
        data.push(temp);
    }

    var collection = new PourOver.Collection(data);
    var filter = PourOver.makeExactFilter("ug", 2.522);
    var result = collection.addFilters(filter);
    console.log(result);

    var end = new Date().getTime();
    var time = end - start;

    infos += "Load data : " + time + "ms <br/>";

    var end2 = new Date().getTime();
    var time = end2 - end;

    infos += "Show data : " + time + "ms <br/>";

    search += "<form name='searchForm' onsubmit='return validateForm()' method='post'><select name='field' size='1'>"

    for (var i = 0; i < label.length; i++) {
        search += "<option>" + label[i];
    }

    search += "</select><input name='value'/><input type='submit' value='submit'/></form>";

    $("#search").html(search);

}

var ftab;

function validateForm() {
    var start = new Date().getTime();
    result = "";
    var field = document.forms["searchForm"]["field"].value;
    var value = document.forms["searchForm"]["value"].value;

    // control if the value is not empty
    if (value == null || value == "") {
        alert("Veuillez entrer une valeur");
    }

    // good case
    else {

        var end = new Date().getTime();
        var time = end - start;

        infos += "Search : " + time + "ms <br/>";

        // print the results
        if (ftab[0].length > 0) {
            result += ftab[0].length + " rows founds (over " + count + " ) <br/>";

            if (ftab[0].length < 10000)
                result += printTable2(ftab);
            else {
                result += "To many rows ; print disabled. <br/>";
            }
        } else {
            result += "No result found ! <br/>";
        }

        $("#result").html(result);

        // performance measures

        repaintInfos();
    }

    return false;
}