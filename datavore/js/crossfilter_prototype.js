/*
	DATAVORE PROTOTYPE
	*/

	var result = "";
	var infos = "";
	var count = 0;

// import data
var input;
function readMultipleFiles(evt) {
	var start = new Date().getTime();
	var files = evt.target.files;
	if (files) {
		for (var i=0, f; f=files[i]; i++) {
			var r = new FileReader();
			r.onloadend = function(e) {
				input = e.target.result;
				process();
				
				var end = new Date().getTime();
				var time = end - start;

				infos += "Load data : "+time+"ms <br/>";
				repaintInfos();
			};
			r.readAsText(f);
		}
	} else {
		alert("Failed to load files");
	}
}

document.getElementById('fileinput').addEventListener('change', readMultipleFiles, false);  

// main function
var label = [];
var data;
var cf;
function process() {
	var content = $.csv.toArrays(input);
	
	var array = [];

	var search = "";
    //content = invertArray(content);
    label = content[0];
    count = content.length-1;
    for(var i = 1 ; i < content.length ; i++) {

    	var tmp = {};
    	
    	for(var j = 0 ; j < content[0].length ; j++) {
    		tmp[label[j]] = content[i][j];
    	}
    	array.push(tmp);
    }

    cf = crossfilter(array);


    search += "<form name='searchForm' onsubmit='return validateForm()' method='post'><select name='field' size='1'>"

    for(var i = 0 ; i < label.length ; i++) {
    	search += "<option>"+label[i];
    }

    search += "</select><input name='value'/><input type='submit' value='submit'/></form>";
    
    $("#search").html(search);
}

function printTable(array) {
	var print = "<table border=1>";
	print += "<tr>"
	for(var i = 0 ; i < label.length ; i++) {
		print += "<th>"+label[i]+"</th>";
	}
	print += "</tr>";
	for(var i = 0 ; i < array[0].length ; i++) {
		print += "<tr>";
		for(var j = 0 ; j < array.length ; j++) {
			//document.write(array);
			print += "<td>"+array[j][i]+"</td>";
		}
		print += "</tr>";
	}
	print += "</table>";
	return print;
}

function validateForm() {
	var start = new Date().getTime();
	result = "";
	var field = document.forms["searchForm"]["field"].value;
	var value = document.forms["searchForm"]["value"].value;

    // control if the value is not empty
    if (value == null || value == "") {
    	alert("Veuillez entrer une valeur");
    }

    // good case
    else {

    	var byField = cf.dimension(function(p) { return eval("p."+field); });
    	byField.filterExact(value);

    	var end = new Date().getTime();
    	var time = end - start;

    	infos += "Search : "+time+"ms <br/>";

    	// print the results
    	if(true) {
    		//result += ftab[0].length+" rows founds (over "+count+" ) <br/>";
    		if(true) {
    			result += "<table border=1>";
    			result += "<tr>"
    			for(var i = 0 ; i < label.length ; i++) {
    				result += "<th>"+label[i]+"</th>";
    			}
    			result += "</tr>";
    			byField.top(Infinity).forEach(function(p, i) {
    				result += "<tr>";
    				Object.keys(p).forEach(function(key) {
    					result += "<td>"+p[key]+"</td>";
    				});
    				result += "</tr>";
    			});
    			
    			result += "</table>";

    		}
    		else {
    			result += "To many rows ; print disabled. <br/>";
    		}
    	}
    	else {
    		result += "No result found ! <br/>";
    	}

    	$("#result").html(result);

    	// performance measures

    	repaintInfos();
    }

    return false;
}

function repaintInfos() {
	$("#infos").html(infos);
}