package main;

/*
 * Main class
 */
public class Main {

	/*
	 * Launch the JNanocubes process
	 */
	public static void main(String[] args) {
		JNanocubes jnano = new JNanocubes();
		jnano.process(args);
	}
	
}
