package main;

import java.util.HashMap;
import java.util.Map;

import objects.Node;
import objects.TreeSchema;

import com.beust.jcommander.JCommander;

import parser.Parser;
import server.ServerLauncher;
import tools.Arguments;
import tools.Query;
import tools.TileGenerator;

/*
 * Main class
 */
public class JNanocubes {
	public static int firstLevel = 3;
	public static Query query;
	public static TreeSchema schema;
	public static String dfmt;
	public static Node tree;
	public static int uniqueKeys;
	public static boolean isOpt;
	public static HashMap<Integer,Integer[]> minMax;
	public static Map<String, Integer> catTab;
	public static String[] keyCat;
	public static int calls = 0;
	public static TileGenerator tileGenerator;
	
	public void process(String[] args) {
		/*
		 * Host and port
		 */
		String host = "130.79.128.184";
		int port = 8080;
		
		/*
		 * Arguments
		 */
		Arguments jct = new Arguments();
		new JCommander(jct, args);
		isOpt = jct.isOpt();
		dfmt = jct.datefmt;
		
		/*
		 * Launching the parser
		 */
		System.out.println("Building the tree..");
		
		keyCat = new String[0];
		catTab = new HashMap<String,Integer>();		
		minMax = new HashMap<Integer, Integer[]>();

		Parser parser = new Parser("data/160000.csv", jct.spatial, jct.categorical, jct.temporal, jct.infos, jct.datefmt, jct.zoom);
		tree = parser.getTree();
		
		System.out.println("Parsing finished");
		
		/*
		 * Query manager
		 */
		query = new Query(tree);
		
		tileGenerator = new TileGenerator(8);
		
		/*
		 * Launching the server
		 */
		new ServerLauncher(host, port);
		
		TileGenerator.clean();

	}
	
}
