package objects;

import java.util.Arrays;
import java.util.Date;

import main.JNanocubes;

import org.apache.commons.lang3.ArrayUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import tools.TreeBuilder;

/*
 * Node class
 */
public class Node implements Comparable<Node> {
	
	/*
	 * Parent
	 */
	private Node parent;
	
	/*
	 * Number of leaves
	 */
	int nbLeaves;

	/*
	 * Children tab
	 */	
	private Node[] children;
		
	/*
	 * Leaves set
	 */
	public Leaf[][] leaves;

	private int[][] catIds;

	private int[][] catPos;
	
	private int[][] catSubPos;
	
	private int[][] catLengths;
	
	// npix
	private int key;
	
	/*
	 * Id
	 */
	private long id;
	
	/*
	 * Level
	 */
	private int level;
	
	/*
	 * Constructor
	 */
	public Node(int key, Node parent) {
		this.id = TreeBuilder.nodesNumber;
		TreeBuilder.nodesNumber++;
		nbLeaves = 0;
		if(parent == null)
			level = 0;
		else
			level = parent.getLevel()+1;
		if(level == 0)
			this.children = new Node[(int) (12*Math.pow(4,JNanocubes.firstLevel))];
		else
			this.children = new Node[0];
		this.key = key;
		this.parent = parent;
	}
	
	/*
	 * Add a node as child
	 */
	public void addChild(Node child) {
		if(level == 0)
			children[child.getKey()] = child;
		else
			this.children = ArrayUtils.add(children, child);
		if(level != 0)
			Arrays.sort(children);
	}
	
	/*
	 * Getter for the level
	 */
	public int getLevel() {
		return this.level;
	}
	
	public int getKey() {
		return key;
	}
	
	/*
	 * Getter for the children
	 */
	public Node getChild(int key) {
		if(level == 0)
			return children[key];
		else
			for(Node n : children)
				if(n.getKey() == key)
					return n;
		return null;
	}
	
	/*
	 * Check if has child
	 */
	public boolean hasChild(int key) {
		if(level == 0) {
			return children[key] != null;
		}
		for(Node n : children)
			if(n.getKey() == key)
				return true;
		return false;
	}
	
	/*
	 * Getter for the children
	 */
	public Node[] getChildren() {
		return children;
	}
	
	/*
	 * Getter for the parent
	 */
	public Node getParent() {
		return parent;
	}
	
	@SuppressWarnings("unchecked")
	public String toString() {
		JSONObject me=new JSONObject();
		JSONObject obj=new JSONObject();
		JSONArray list;
		if(leaves != null) {
			list = new JSONArray();
			for(Leaf leaf : leaves[0])
				list.add(leaf);
			obj.put("leaves", list);
		}
		//else if(!children.isEmpty()) {
			list = new JSONArray();
			for(Object o : children) {
				list.add((Node)o);
			}
			obj.put("children", list);
		//}
		me.put(getKey(), obj);
		return me.toJSONString();
	}
	
	/*
	 * Get all the entry
	 */
	public Leaf[] getAll() {
		return leaves[0];
	}
	
	public int getCount() {
		return nbLeaves;
	}
	
	@SuppressWarnings("unchecked")
	public String getLeaves() {
		JSONObject re = new JSONObject();
		JSONArray list = new JSONArray();
		if(leaves.length != 0) {
			list = new JSONArray();
			for(Leaf leaf : leaves[0])
				list.add(leaf);
		}
		re.put("result", list);
		re.put("count", nbLeaves);
		return re.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public String query(String category, Date from, Date to) {
		JSONObject re = new JSONObject();
		
		long start = System.currentTimeMillis();
		JSONArray list = getLeaves(category, from, to);
        long last = System.currentTimeMillis()-start;
        System.out.println("## Query in "+last+" ms.");
		//re.put("result", list);
		re.put("count", list.size());
		return re.toJSONString();
	}

	@SuppressWarnings("unchecked")
	public int queryCount(String category, Date from, Date to) {
		JSONObject re = new JSONObject();

		JSONArray list = getLeaves(category, from, to);
		return list.size();
	}
	
	@SuppressWarnings("unchecked")
	public JSONArray getLeaves(String category, Date from, Date to) {
		JSONArray list = new JSONArray();
		
		if(leaves == null) {
			for(Node n : children)
				if(n != null) {
					list.addAll(n.getLeaves(category, from, to));
				}
		}
		else {
		int catIndex, cPos, cLength;
		if(category != null && JNanocubes.catTab.containsKey(category)) {
			catIndex = JNanocubes.catTab.get(category);
			int k = ArrayUtils.indexOf(catIds[catIndex], ArrayUtils.indexOf(JNanocubes.keyCat,category));
			if(k != -1) {
				cPos = catPos[catIndex][k];
				cLength = catLengths[catIndex][k];
			}
			else {
				cPos = 0;
				cLength = 0;
			}
		}
		else {
			catIndex = 0;
			cPos = 0;
			cLength = leaves[catIndex+1].length;
		}

		if(leaves.length != 0) {
			
			int fromIndex = -1, toIndex = -1;
			if(from == null) {
				fromIndex = 0;
			}
			else {
				for(int i = cPos ; i < cPos+cLength ; i++) {
					if(leaves[catIndex+1][i].getDate() >= from.getTime()) {
						fromIndex = i;
						break;
					}
				}
			}
			if(to == null) {
				toIndex = leaves[catIndex+1].length-1;
			}
			else {
				for(int i = cLength+cPos-1 ; i >= cPos ; i--) {
					if(leaves[catIndex+1][i].getDate() <= to.getTime()) {
						toIndex = i+1;
						break;
					}
				}
				if(toIndex == -1)
					toIndex = fromIndex;
				if(fromIndex == -1) {
					toIndex = 0;
					fromIndex = 0;
				}
			}

			if(fromIndex >= 0 && toIndex >=0) {
				Leaf[] resArray = Arrays.copyOfRange(leaves[catIndex+1], fromIndex, toIndex);
				for(Leaf l : resArray)
					list.add(l);
			}
		}
		}
		return list;
	}
	
	public void addToCount() {
		nbLeaves++;

		if(!JNanocubes.minMax.containsKey(level)) {
			JNanocubes.minMax.put(level, new Integer[] {-1,0});
		}
		else {
			if(JNanocubes.minMax.get(level)[0] == -1 || JNanocubes.minMax.get(level)[0] > nbLeaves) {
				JNanocubes.minMax.get(level)[0] = nbLeaves;
			}
			if(JNanocubes.minMax.get(level)[1] < nbLeaves) {
				JNanocubes.minMax.get(level)[1] = nbLeaves;
			}
		}
		
		if(parent != null)
			parent.addToCount();
	}
	
	public void addLeaf(Leaf leaf) {
		if(parent != null) {
		JNanocubes.calls++;
		/*
		 * Add the leaf to the array, sort 
		 */	
	    
		if(parent != null)
			parent.addToCount();
		
	    if(leaves == null) {
	    	this.leaves = new Leaf[0][];
			leaves = ArrayUtils.add(leaves, new Leaf[0]);
			this.catIds = new int[0][];
			this.catPos = new int[0][];
			this.catLengths = new int[0][];
	    }
	    
	    String[] cats = leaf.getCats();
	    
    	//System.out.println("#### "+key);
	    
    	// iterate leaves categories
	    for(int c = 0 ; c < cats.length ; c++) {
	    	String s = cats[c];
	    	
	    	//System.out.println("# "+s);
	    	int j = 0;
	    	
	    	int i = JNanocubes.catTab.get(s); 
	    	//System.out.println("-> "+i);
			while(leaves.length <= i+1) {
				leaves = ArrayUtils.add(leaves, new Leaf[0]);
			}
			while(catIds.length <= i) {
				catIds = ArrayUtils.add(catIds, new int[0]);
				catPos = ArrayUtils.add(catPos, new int[0]);
				catLengths = ArrayUtils.add(catLengths, new int[0]);
			}

			if(!ArrayUtils.contains(catIds[i], ArrayUtils.indexOf(JNanocubes.keyCat,s))) {
				catIds[i] = ArrayUtils.add(catIds[i], ArrayUtils.indexOf(JNanocubes.keyCat,s));
				catLengths[i] = ArrayUtils.add(catLengths[i], 0);
				catPos[i] = ArrayUtils.add(catPos[i], leaves[i+1].length);
			}
	    	
			j = ArrayUtils.indexOf(catIds[i], ArrayUtils.indexOf(JNanocubes.keyCat,s));
			
	    	// start position of that cat 
	    	int position = catPos[i][j];
	    	leaves[i+1] = ArrayUtils.add(leaves[i+1], position, leaf);
	    	catLengths[i][j]++;

	    	for(int k = j+1 ; k < catPos[i].length ; k++)
	    		catPos[i][k]++;
	    	
	    	// At the end, we sort the subarray
	    	Arrays.sort(leaves[i+1], position, position+catLengths[i][j]);
	    	
	    	/*
	    	 * Combined categories arrays
	    	 */
	    	if(leaves.length > 1) {
	    		if(catSubPos == null) {
	    			this.catSubPos = new int[0][];
	    		}
	    	}
	    	
	    }
	    
    	leaves[0] = ArrayUtils.add(leaves[0], leaves[0].length, leaf);
	    Arrays.sort(leaves[0]);
		}
	    nbLeaves++;
	}
	
	public long getId() {
		return this.id;
	}
	
	@Override
	public boolean equals(Object o) {
		return this.id == ((Node)o).getId();
	}

	@Override
	public int compareTo(Node o) {
		return ((Integer)this.key).compareTo(o.getKey());
	}
}
