package objects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/*
 * TreeSchema class
 */
public class TreeSchema {
	
	/*
	 * Natures
	 */
	public enum Nature {SPATIAL, CATEGORICAL, TEMPORAL} ;
	
	/*
	 * Date format
	 */
	SimpleDateFormat datefmt;
	
	/*
	 * Zoom level number
	 */
	int zoom;
	
	/*
	 * Spatial labels
	 */
	private Map<String,ArrayList<Integer>> labels;
	
	/*
	 * Int to label
	 */
	private Map<Integer,String> IdToLabel;	
	
	/*
	 * Constructor
	 */
	public TreeSchema() {
		this.labels = new HashMap<String,ArrayList<Integer>>();
		this.labels.put("spatial", new ArrayList<Integer>());
		this.labels.put("categorical", new ArrayList<Integer>());
		this.labels.put("temporal", new ArrayList<Integer>());
		this.labels.put("infos", new ArrayList<Integer>());
		this.datefmt =  new SimpleDateFormat("dd-MMM-yyyy");
		
		this.IdToLabel = new HashMap<Integer,String>();
		
		this.zoom = 5;
	}
	
	public void addIdLabel(int id, String label) {
		this.IdToLabel.put(id, label);
	}
	
	public String getIdLabel(int id) {
		return this.IdToLabel.get(id);
	}
	
	/*
	 * Set date format
	 */
	public void setDatefmt(String fmt) {
		this.datefmt =  new SimpleDateFormat(fmt);
	}
	
	/*
	 * Add a spatial label
	 */
	public void addSpatial(int id) {
		labels.get("spatial").add(id);
	}
	
	/*
	 * Add a type label
	 */
	public void addType(int id) {
		labels.get("categorical").add(id);
	}
	
	/*
	 * Add a time label
	 */
	public void addTime(int id) {
		labels.get("temporal").add(id);
	}
	
	/*
	 * Add a info label
	 */
	public void addInfo(int id) {
		labels.get("infos").add(id);
	}
	
	/*
	 * Returns the numbers of spatial labels
	 */
	public int getSpatialCount() {
		return labels.get("spatial").size();
	}
	
	/*
	 * Returns the number of type labels
	 */
	public int getTypeCount() {
		return labels.get("categorical").size();
	}
	
	/*
	 * Returns the number of info labels
	 */
	public int getInfoCount() {
		return labels.get("infos").size();
	}
	
	/*
	 * Returns the number of time labels
	 */
	public int getTimeCount() {
		return labels.get("temporal").size();
	}
	
	public ArrayList<Integer> getSpatial() {
		return labels.get("spatial");
	}
	
	public ArrayList<Integer> getCategorical() {
		return labels.get("categorical");
	}
	
	public ArrayList<Integer> getTemporal() {
		return labels.get("temporal");
	}
	
	public ArrayList<Integer> getInfos() {
		return labels.get("infos");
	}
	
	public int getZoom() {
		return this.zoom;
	}
	
	public boolean isSpatial(int id) {
		return labels.get("spatial").contains(id);
	}
	
	public boolean isCategorical(int id) {
		return labels.get("categorical").contains(id);
	}
	
	public boolean isTemporal(int id) {
		return labels.get("temporal").contains(id);
	}
	
	public boolean isInfo(int id) {
		return labels.get("infos").contains(id);
	}
	
	public void setSpatial(int[] cols) {
		labels.get("spatial").clear();
		for(int i : cols)
			labels.get("spatial").add(i);
	}
	
	public void setCategorical(int[] cols) {
		labels.get("categorical").clear();
		for(int i : cols)
			labels.get("categorical").add(i);
	}
	
	public void setTemporal(int[] cols) {
		labels.get("temporal").clear();
		for(int i : cols)
			labels.get("temporal").add(i);
	}
	
	public void setInfos(int[] cols) {
		labels.get("infos").clear();
		for(int i : cols)
			labels.get("infos").add(i);
	}
	
	public void setZoom(int zoom) {
		this.zoom = zoom;
	}
	
	/*
	 * Get date parsed
	 */
	public Date parseDate(String s) {
		try {
			return datefmt.parse(s);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String getDatefmt() {
		return datefmt.toPattern();
	}
	
	public boolean isCol(int id) {
		return isSpatial(id)
			|| isCategorical(id)
			|| isTemporal(id)
			|| isInfo(id);
	}
	
	public boolean isTypeNode(int id) {
		return id > this.getSpatialCount() && id <= this.getSpatialCount()+this.getTypeCount();
	}

	public String toString() {
		String r = "";
		r += "spatial : ";
		for(int i : getSpatial())
			r += i+",";
		r += "/ categorical : ";
		for(int i : getCategorical())
			r += i+",";
		r += "/ temporal : ";
		for(int i : getTemporal())
			r += i+",";
		r += "/ info : ";
		for(int i : getInfos())
			r += i+",";
		return r;
	}

}
