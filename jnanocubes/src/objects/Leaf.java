package objects;

import java.util.Date;

import org.apache.commons.lang3.ArrayUtils;

import main.JNanocubes;
import tools.TreeBuilder;

public class Leaf implements Comparable<Leaf> {
	private long id;
	private Long date;
	int[] cats;
	
	public Leaf(Date date) {
		this.id = TreeBuilder.leavesNumber;
		TreeBuilder.leavesNumber++;
		this.date = date.getTime();
	}
	
	public String toString() {
		return id+" ("+(new Date(date).getYear()+1900)+")";
	}
	
	public boolean equals(Object o) {
		return this.id == ((Leaf)o).getId();
	}
	
	public long getId() {
		return id;
	}
	
	@Override
	public int compareTo(Leaf o) {
		return date.compareTo(o.getDate());
	}
	
	public Long getDate() {
		return date;
	}
	
	public String[] getCats() {
		String[] t = new String[cats.length];
		for(int i = 0 ; i < cats.length ; i++)
			t[i] = JNanocubes.keyCat[cats[i]];
		return t;
	}
	
	public void setCats(String[] source) {
		this.cats = new int[source.length];
		for(int i = 0 ; i < source.length ; i++) {
			cats[i] = ArrayUtils.indexOf(JNanocubes.keyCat, source[i]);
		}
	}
}
