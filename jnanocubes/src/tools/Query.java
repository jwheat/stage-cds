package tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import main.JNanocubes;
import objects.Node;

/*
 * Query class
 */
public class Query {
	
	/*
	 * Tree
	 */
	Node tree;
	
	/*
	 * Constructor
	 */
	public Query(Node tree) {
		this.tree = tree;
	}
	
	/*
	 * Query method
	 */
	@SuppressWarnings("unchecked")
	public String query(Map<String,String> params) {
		String result = "";
		Node currentTree = tree;
		boolean notfound = false;
				
        // spatial
        if (params.get("spatial") != null) {
        	String[] coords = params.get("spatial").split(",");
        	for(String s : coords) {
        		if(currentTree.hasChild(Integer.parseInt(s)))
        			currentTree = currentTree.getChild(Integer.parseInt(s));
        		else
        			notfound = true;
        	}
        }

      	Date from = null,to = null;
   		try {
   			SimpleDateFormat datefmt = new SimpleDateFormat(JNanocubes.dfmt);
   			if(params.get("from") != null)
   				from = datefmt.parse(params.get("from"));
   			else
   				from = null;
   			
   			if(params.get("to") != null)
   				to = datefmt.parse(params.get("to"));
   			else
   				to = null;
   			
   		} catch (ParseException e) {
   			e.printStackTrace();
   		}
 
   		result = currentTree.query(params.get("categorical"), from, to);
 
       
		JSONObject re = new JSONObject();
		JSONArray list = new JSONArray();
		re.put("count", 0);
		re.put("result", list);
        
		// if not found
        if(notfound)
        	result = re.toJSONString();
        
        return result;
	}
	
}
