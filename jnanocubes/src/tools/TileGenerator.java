package tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

import objects.Node;
import main.JNanocubes;
import cds.fits.Fits;

/*
 * Tile generator
 */
public class TileGenerator {
	int max, min;
	Node tree = JNanocubes.tree;
	int tileWidth = 32;
	int nordermax;
	
	public TileGenerator(int nordermax) {
		this.nordermax = nordermax;
		process(tree,3, null, null, null);
	}
	
	public void gentiles(Map<String, String> params) {
		
	}

	public void process(Node tree, int norder, String category, Date from, Date to) {
		if(norder < nordermax) {

		min = JNanocubes.minMax.get(norder-JNanocubes.firstLevel+1)[0];
		max = JNanocubes.minMax.get(norder-JNanocubes.firstLevel+1)[1];

		// System.out.println("min : " +min);
		// System.out.println("max : " +max);
		//int sum = 0;
		//int zeros = 0;
		for(Object o : tree.getChildren()) {
			Node node = (Node)o;
			if(node != null) {
				//System.out.println("------------");
				//System.out.println("noeud : "+node.getKey());
				//System.out.println("count : "+node.queryCount(null, null, null));
				//System.out.println("scale "+ scale(node.queryCount(null, null, null)));
				//System.out.println("------------");
				// if(node.queryCount(null, null, null)==0)
				// zeros++;
				generate(scale(node.queryCount(null, null, null)), node.getKey(), norder);
				// sum += node.queryCount(null, null, null);
				
				//process(node, norder+1);
			}
		}
		//System.out.println("SUM : "+sum);
		//System.out.println("ZEROS : "+zeros);
		}
	}
	
	public int scale(int value) {
		return (int)((255+10) * (value-min) / (max-min))+10;
	}
	
	/*
	 * Process method
	 */
    public Fits generate(int value, int npix, int norder) {
		Fits tile = new Fits(tileWidth, tileWidth, 8);
		
        for(int i = 0 ; i < tile.width ; i++) {
            for(int j = 0 ; j < tile.width ; j++) {
            	tile.setPixelInt(i, j, value);
            }
        }
        
        export(tile, norder, npix);
        return tile;
    }
    
    /*
     * Export method
     */
    public void export(Fits tile, int norder, int npix) {
        try {
        	int dirId = (npix/10000)*10000;
        	File survey = new File("web/survey");
        	survey.delete();
        	
        	File f = new File(survey+"/Norder"+norder+"/Dir"+dirId+"");
        	
        	f.mkdirs();
            FileOutputStream fos = new FileOutputStream(f+"/Npix"+npix+".jpg");
            
            // 0-255 : range of values
            tile.writeCompressed(fos, 0, 255, null, "jpeg");
            
            fos.close();
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
