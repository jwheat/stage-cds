package tools;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.internal.Lists;

/*
 * Arguments class
 */
public class Arguments {
    @Parameter
    public List<String> parameters = Lists.newArrayList();

    @Parameter(names = "-spatial", variableArity = true, description = "Spatial fields (ra, dec)", required = true)
    public List<String> spatial = new ArrayList<String>();
    
    @Parameter(names = "-categorical", variableArity = true, description = "Categorical fields", required = false)
    public List<String> categorical = new ArrayList<String>();
    
    @Parameter(names = "-temporal", description = "Temporal fields", required = true)
    public String temporal;
    
    @Parameter(names = "-info", variableArity = true, description = "Additional info fields", required = false)
    public List<String> infos = new ArrayList<String>();

    @Parameter(names = "-datefmt", description = "Date format", required = true)
    public String datefmt;
    
    @Parameter(names = "-zoom", description = "Zoom max level", required = true)
    public int zoom;
    
    @Parameter(names = "-port", description = "Port")
    public int port;
    
    @Parameter(names = "-opt", description = "Optimal memory")
    public boolean opt = false;

    public boolean isOpt() {
    	return opt;
    }
}