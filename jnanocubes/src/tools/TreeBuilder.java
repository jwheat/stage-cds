package tools;

import java.util.ArrayList;

import main.JNanocubes;
import objects.Leaf;
import objects.Node;
import objects.TreeSchema;

/*
 * TreeBuilder ; This class builds a tree from the data provided
 */
public class TreeBuilder {
	
	/*
	 * Nodes number
	 */
	public static int nodesNumber = 0;
	
	/*
	 * Leaves number
	 */
	public static int leavesNumber = 0;
	
	/*
	 * Tree built
	 */
	private Node tree;

	/*
	 * Input schema
	 */
	private TreeSchema schema;

	/*
	 * Constructor ; initialize the tree
	 */
	public TreeBuilder(TreeSchema schema) {
		this.tree = new Node(0, null);
		this.schema = schema;
	}

	/*
	 * Algorithm for building
	 */
	public void process(String[] row) {
		Node currentTree = tree;
		String[] values;
		
		ArrayList<Node> leafers = new ArrayList<Node>();
		ArrayList<String> catHistory = new ArrayList<String>();
		// iterate each column value
		for (int i = 0; i < row.length; i++) {
			String s = row[i];

			/*
			 * SPATIAL
			 */
			if(schema.isSpatial(i)) {				
				if (!currentTree.hasChild(Integer.parseInt(s))) {
					Node tmp = new Node(Integer.parseInt(s), currentTree);
					currentTree.addChild(tmp);
				}
				
				currentTree = currentTree.getChild(Integer.parseInt(s));
				
				if(JNanocubes.isOpt) {
					if(!leafers.contains(currentTree))
						leafers.add(currentTree);

					Node parent = currentTree.getParent();
				
					while (parent != null) {
						if(!leafers.contains(parent))
							leafers.add(parent);
						parent = parent.getParent();
					}
				}
				
			}
			
			/*
			 * CATEGORICAL
			 */
			if(schema.isCategorical(i)) {
				catHistory.add(s);
				// currentTree = currentTree.getChild(s);
			}

			/*
			 * TEMPORAL ->> The end, create leaves
			 */
			if (schema.isTemporal(i)) {
				Leaf leaf;
				
				// add values to leaf
				values = new String[row.length];

				for (int j = 0; j < row.length; j++) {
					values[j] = row[j];
				}
				// create leaf
				leaf = new Leaf(schema.parseDate(s));
				
				// set the categories
				leaf.setCats(catHistory.toArray(new String[catHistory.size()]));
				
				// add leaf to the nodes
				
				if(JNanocubes.isOpt) {
					for (Node n : leafers) {
						n.addLeaf(leaf);
					}
				}
				else {
					currentTree.addLeaf(leaf);
				}
			}

		}

	}

	/*
	 * Getter for the tree
	 */
	public Node getTree() {
		return tree;
	}
}
