package parser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import main.JNanocubes;
import objects.Node;
import objects.TreeSchema;

import org.apache.commons.lang3.ArrayUtils;
import org.asterope.healpix.PixTools;
import org.asterope.healpix.PixToolsNested;

import tools.TreeBuilder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import au.com.bytecode.opencsv.CSVReader;

/*
 * Parser class
 * Builds the tree schema, parse the input file, and launch the tree builder
 */
public class Parser {
	
	/*
	 * Tree schema
	 */
	TreeSchema schema;
	
	/*
	 * Tree builder
	 */
	TreeBuilder builder;
	
	/*
	 * Objects processed number
	 */
	int nbObjects;

	PixTools tool;
	
	/*
	 * Constructor
	 */
	public Parser(String path, List<String> spatial, List<String> categorical, String temporal, List<String> info, String datefmt, int zoom) {
		parse(path, spatial, categorical, temporal, info, datefmt, zoom);
		//write();
		end();
	}

	/*
	 * Main method : parsing
	 */
	public void parse(String path, List<String> spatial, List<String> categorical, String temporal, List<String> infos, String datefmt, int zoom) {
		CSVReader reader;

		try {
			// CSV reader
			reader = new CSVReader(new FileReader(path));

			// nextline
			String[] nextLine;

			// useful arraylist
			final ArrayList<String> temp = new ArrayList<String>();

			// polar coordinates
			double[] polar = new double[2];

			// build the builder schema
			TreeSchema builderSchema = new TreeSchema();
			JNanocubes.schema = builderSchema;
			
			// build the schema
			this.schema = new TreeSchema();
			
			// read the first line (labels)
			nextLine = reader.readNext();
			
			// build the schema
			for(int k = 0 ; k < nextLine.length ; k++) {
				String tmp = nextLine[k];

				for(String s : spatial)
					if(s.equals(tmp)) {
						schema.addSpatial(k);
						schema.addIdLabel(k, tmp);
					}
				for(String s : categorical)
					if(s.equals(tmp)) {
						schema.addType(k);
						schema.addIdLabel(k, tmp);
					}
				if(temporal.equals(tmp)) {
					schema.addTime(k);
					schema.addIdLabel(k, tmp);
				}
				for(String s : infos)
					if(s.equals(tmp)) {
						schema.addInfo(k);
						schema.addIdLabel(k, tmp);
					}
			}

			// set date format
			schema.setDatefmt(datefmt);
			
			// set zoom
			schema.setZoom(zoom);
			
			int f = 0;
			for(int z = 3 ; z <= schema.getZoom() ; z++) {
				builderSchema.addSpatial(f);
				builderSchema.addIdLabel(f, "norder-"+(z));
				f++;
			}
			
			for(int k = 0 ; k < schema.getTypeCount() ; k++) {
				builderSchema.addType(f);
				builderSchema.addIdLabel(f, schema.getIdLabel(schema.getCategorical().get(k)));
				f++;
			}

			builderSchema.addTime(f);
			builderSchema.addIdLabel(f, "time");
			f++;
			
			for(int k = 0 ; k < schema.getInfoCount() ; k++) {
				builderSchema.addInfo(f);
				builderSchema.addIdLabel(f, schema.getIdLabel(schema.getInfos().get(k)));
				f++;
			}
			
			builderSchema.setDatefmt(schema.getDatefmt());
			this.builder = new TreeBuilder(builderSchema);
			
			// time measures
			long start = System.currentTimeMillis();
			long start2 = System.currentTimeMillis();
			boolean empty;
			
			ArrayList<String> spatio = new ArrayList<String>();
			ArrayList<String> cat = new ArrayList<String>();
			ArrayList<String> tempo = new ArrayList<String>();
			ArrayList<String> info = new ArrayList<String>();
			
	        int mb = 1024*1024;
			this.nbObjects = 0;
			
	        //Getting the runtime reference from system
	        Runtime runtime = Runtime.getRuntime();
	        
			/*
			 * Read each line
			 */
			while ((nextLine = reader.readNext()) != null) {
				empty = false;
				temp.clear();
				
				/*
				 * logging
				 */

				if (nbObjects % 100000 == 0 & nbObjects != 0) {

					System.out.println(nbObjects + " objects ("
							+ (System.currentTimeMillis() - start) / 1000
							+ " s / "+(System.currentTimeMillis() - start2) / 1000+" s) [" + TreeBuilder.nodesNumber + " nodes]"
							+ " [" + TreeBuilder.leavesNumber + " leaves] "
							+ "[memory : " + (runtime.totalMemory()-runtime.freeMemory()) / mb+" MB]");
					start = System.currentTimeMillis();
				}
				nbObjects++;

				/*
				 * process each element of the row and update the tree
				 */
				spatio.clear();
				cat.clear();
				tempo.clear();
				info.clear();
				
				for (int i = 0; i < nextLine.length; i++) {					
					// test if empty
					if (nextLine[i].isEmpty()) {
						empty = true;
						break;
					}
					// if is a column
					else if (schema.isCol(i)) {
						// process ra and dec
						if (i == schema.getSpatial().get(0))
							polar[0] = Double.parseDouble(nextLine[i]);
						else if (i == schema.getSpatial().get(1)) {
							polar[1] = Double.parseDouble(nextLine[i]);
							long hp = toHealpix(polar[0], polar[1],schema.getZoom());
							spatio.add(hp+"");
							for(int z = schema.getZoom()-1 ; z >= 3 ; z--) {
								hp = hp/4;
								spatio.add(hp+"");
							}
							Collections.reverse(spatio);
						} 
						if(schema.isCategorical(i)) {
							cat.add(nextLine[i]);
						}
						if(schema.isTemporal(i)) {
							tempo.add(nextLine[i]);
						}
						if(schema.isInfo(i)) {
							info.add(nextLine[i]);
						}
					}

				}
				
				// update the tree
				if (!empty) {
					temp.addAll(spatio);
					temp.addAll(cat);
					temp.addAll(tempo);
					temp.addAll(info);

					for(int i = 0 ; i < cat.size() ; i++) {
						if(!JNanocubes.catTab.containsKey(cat.get(i)))
							JNanocubes.catTab.put(cat.get(i), i);
						if(!ArrayUtils.contains(JNanocubes.keyCat, cat.get(i)))
							JNanocubes.keyCat = ArrayUtils.add(JNanocubes.keyCat, cat.get(i));
					}

					builder.process(temp.toArray(new String[temp.size()]));
				}
			}
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * To json (tree.json)
	 */
	public void write() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(this.getTree().toString());
		String prettyJsonString = gson.toJson(je);
		
		PrintWriter writer;
		try {
			writer = new PrintWriter("tree.json", "UTF-8");
			writer.println(prettyJsonString);
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * End message
	 */
	public void end() {
        int mb = 1024*1024;
		
        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();
        
		System.out.println("## Finished : "+nbObjects+" objects [" + TreeBuilder.nodesNumber + " nodes]"
				+ " [" + TreeBuilder.leavesNumber + " leaves] [memory : " + (runtime.totalMemory()-runtime.freeMemory()) / mb+" MB]");
	}
	
	/*
	 * To healpix conversion
	 */
	public long toHealpix(double ra, double dec, int norder) {
		int nside = (int) Math.pow(2, norder);

		double[] polar = { 0.0, 0.0 };

		double theta = Math.PI / 2. - Math.toRadians(dec);
		double phi = Math.toRadians(ra);

		polar[0] = theta;
		polar[1] = phi;

		tool = new PixTools(nside);

		long ipixRing = tool.ang2pix(polar[0], polar[1]);

		long ipixNested = PixToolsNested.ring2nest(nside, ipixRing);
		
		return ipixNested;
	}

	/*
	 * Getter for the tree
	 */
	public Node getTree() {
		return builder.getTree();
	}

}
